/* SPDX-License-Identifier: GPL-2.0 */
/* architectural status code for SEAMCALL */

#ifndef __KVM_X86_TDX_ERRNO_H
#define __KVM_X86_TDX_ERRNO_H

#define TDX_SEAMCALL_STATUS_MASK		0xFFFFFFFF00000000ULL

/*
 * TDX SEAMCALL Status Codes (returned in RAX)
 */
#define TDX_SUCCESS				0x0000000000000000ULL
#define TDX_NON_RECOVERABLE_VCPU		0x4000000100000000ULL
#define TDX_INTERRUPTED_RESUMABLE		0x8000000300000000ULL
#define TDX_OPERAND_BUSY			0x8000020000000000ULL
#define TDX_VCPU_NOT_ASSOCIATED			0x8000070200000000ULL
#define TDX_KEY_GENERATION_FAILED		0x8000080000000000ULL
#define TDX_KEY_STATE_INCORRECT			0xC000081100000000ULL
#define TDX_KEY_CONFIGURED			0x0000081500000000ULL
#define TDX_NO_HKID_READY_TO_WBCACHE		0x0000082100000000ULL
#define TDX_EPT_WALK_FAILED			0xC0000B0000000000ULL

/*
 * TDG.VP.VMCALL Status Codes (returned in R10)
 */
#define TDG_VP_VMCALL_SUCCESS			0x0000000000000000ULL
#define TDG_VP_VMCALL_RETRY			0x0000000000000001ULL
#define TDG_VP_VMCALL_INVALID_OPERAND		0x8000000000000000ULL
#define TDG_VP_VMCALL_TDREPORT_FAILED		0x8000000000000001ULL

/*
 * TDX module operand ID, appears in 31:0 part of error code as
 * detail information
 */
#define TDX_OPERAND_ID_RCX			0x01
#define TDX_OPERAND_ID_SEPT			0x92
#define TDX_OPERAND_ID_TD_EPOCH			0xa9

#endif /* __KVM_X86_TDX_ERRNO_H */
